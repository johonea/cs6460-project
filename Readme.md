# AI in Aeromechanics
-------------------------------------

## Introduction

Welcome to the course on AI in Aeromechanics. This course will be covering different AI techniques that can be applied to standard aeromechanical testing. This course will cover content in both the pre-test and post-test sides of analysis. This course is meant for engineers currently in the field of aeromechanical testing and assumes knowledge in aeromechanics.

## Course Layout

### Section 1: Welcome
1. Welcome to AI in Aeromechanics
2. Course Structure
3. What is AI?

### Section 2: Pre-Test Analysis
1. Section Overview and Goals
2. Review: Finite Element Method

> #### Lesson 1: Genetic Algorithms
> 1. Introduction
> 2. The Algorithm
> 3. Visual Example: 8 Queens Problem
> 4. Aeromechanics Example: Find optimum Sensor location Based on Parameters
> 5. Review 
> 6. Resources

> #### Lesson 2: EM With Gaussian Mixture Models
> 1. Introduction
> 2. The Algorithm
> 3. Visual Example: Image Decomposition
> 4. Aeromechanics Example: Find optimum Sensor location Based on Parameters
> 5. Review
> 6. Resources

### Section 3: Data Analysis
1. Section Overview and Goals
2. Review: Data Acquisition and Processing

> #### Lesson 1: Hidden Markov Models
> 1. Introduction
> 2. The Algorithm
> 3. Visual Example: Rainy or Sunny?
> 4. Aeromechanics Example: Recognize Engine Order Vibrational Signiture
> 5. Review and Additional Resources
> 6. Resources

> #### Lesson 2: Neural Networks
> 1. Introduction
> 2. The Algorithm
> 3. Visual Example: A Simple Neural Net
> 4. Aeromechanics Example: Detect Vibratory Event Using Neural Nets and Campbell Collection Grid
> 5. Review
> 6. Resources

> #### Lesson 3: Decision Trees
> 1. Introduction
> 2. The Algorithm
> 3. Visual Example: Boolean Logic
> 4. Aeromechanics Example: Classification
> 5. Review
> 7. Resources

## Access

This course content can be accessed in 2 different ways. The first is by manually using Jupyter Notebooks on your local machine. The second is accessing the public site connected to this Github Repository. Please see the directions below:

#### Access Via Binder

Follow the following link which will bring you to the top level directory of this course. Then browse through the content in the order as presented. All course content is in Jupyter Notbook files with the extension *.ipynb*

https://mybinder.org/v2/gh/johonea4/AI-Aeromechanics-Notebook/master

#### Access Via Manual Set-Up

The project can be accessed by cloning the git repository at https://github.gatech.edu/jhonea3/cs6460project.git. After cloning, you will need to run a Jupyter Notebook server. Please follow the directions below for manual set-up: 

> 1. Download and Install Anaconda for Python 3.6: https://www.anaconda.com/download/ 
> 2.  Run the command: “jupyter notebook” on your command line 
> 3. This should start a notebook server and open a web page. 
> 4. Using the webpage, browse to the directory where you cloned the repository 
> 5. The sections and lessons are ordered in order top to bottom. Step through the folders, open, read, and experiment with the provided notebooks