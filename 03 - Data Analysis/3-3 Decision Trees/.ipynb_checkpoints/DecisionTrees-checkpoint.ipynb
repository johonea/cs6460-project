{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Lesson 3.3 Decision Trees\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Decision tree induction is the simplest and most successful forms of machine learning. Decision trees represent a function that takes as input a vector of attribute values and returns a decision. The decision is reached by executing a sequence of tests. Each internal node in the tree corresponds to a test of the value of one of the input attributes, $A_{i}$ and the branches from the node are labelled with the possible values of the attribute, $A_{i}=V_{ik}$. Decision trees are used for making a decision based on the given attributes, but are also used for classification based on the given attributes."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The Algorithm"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For decision trees to work appropriately, they must first be trained on the attributes they need to decide on. For example, we will take the following table of attributes as examples to give to the tree to learn. This example's purpose is for deciding whether to wait at a restaurant given several attributes or factors.\n",
    "\n",
    "![DtTable](./images/decisionTreeTable.PNG)\n",
    "\n",
    "Given these examples, the decision tree must learn from them and must be able to determine the most significant attribute at each level of the tree. The most significant attribute is needed to ensure the smallest tree possible and to ensure the correct order of node dependancies. The pseudo code for this can be seen below:\n",
    "\n",
    "```python\n",
    "def DT_Learn(examples, attributes, default):\n",
    "    if len(examples) <= 0:\n",
    "        return default\n",
    "    if all examples have same classification:\n",
    "        return classification\n",
    "    if len(attributes) <= 0:\n",
    "        return MODE(examples)\n",
    "    best = Choose-Attribute(attributes,examples)\n",
    "    tree = new decision tree with root test best\n",
    "    for value in best:\n",
    "        examples = {elements of examples with best = value}\n",
    "        subtree = DT_Learn(examples, attributes - best, MODE(examples))\n",
    "        add branch to tree with label value and subtree subtree\n",
    "    return tree\n",
    "```\n",
    "Here, the function ***Choose-Attribute()*** chooses the most significant attribute to switch on. How will be covered later. The function ***MODE()*** returns the most common classification given the set of examples. The ***DT_Learn()*** function will run recursively until the tree has been completely built. At this point, the tree will be returned and can now be used for decision processing. An example decision tree built from these attributes can be seen below:\n",
    "\n",
    "![DTGraph](./images/decisionTree.PNG)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### How is the Most Significant Attribute Chosen?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The most significant attributes are chosen by measuring the *Entropy* of each split and using it to calculate *Information Gain*. Another method is to measure the *Gini Impurity* instead of entropy in order to calculate the *Information Gain*.\n",
    "\n",
    "#### Measuring Entropy:\n",
    "\n",
    "Information entropy is the average rate at which information is produced by a stochastic source of data. The formula for entropy is shown below:\n",
    "\n",
    "$$ H(T) = I_{E}(p_{1},p_{2},...,p_{j}) = -\\sum_{i=1}^{j}p_{i}log_{2}p_{i} $$\n",
    "\n",
    "where $p_{1},p_{2},...$ are percentages of each class present in the child node that results from a split in the tree. All percentages should add together to equal 1.\n",
    "\n",
    "#### Measuring Gini Impurity\n",
    "\n",
    "Gini Impurity is the measure of how often a randomly chosen element from the set would be incorrectly labelled if it was randomly labeled according to the distribution of labels in the subset. This can be computed by summing the probability $p_{i}$, of an item with label *i* being chosen times the probability $\\sum_{k \\ne i} p_{k} = 1 - p_{i}$ of a mistake in categorizing that item. The Gini Impurity reaches the minimum when all cases in the node fall into a single target category.\n",
    "\n",
    "For a set of items with *j* classes, the formula for Gini Impurity is:\n",
    "\n",
    "$$ I_{G}(p) = \\sum_{i=1}^{j}p_{i}\\sum_{k \\ne i}p_{k} = \\sum_{i=1}^{j}p_{i}(1-p_{i}) = \\sum_{i=1}^{j}(p_{i} - p_{i}^{2}) = \\sum_{i=1}^{j}p_{i} - \\sum_{i=1}^{j}p_{i}^{2} = 1 - \\sum_{i=1}^{j}p_{i}^{2} $$\n",
    "\n",
    "#### Calculating the Information Gain\n",
    "\n",
    "Information Gain is the measure of change in information entropy. Gini Impurity can be used as well where the Information Gain (or in this case Gini Gain) is the measure of change in the Gini Impurity. Letting *T* represent a training set of data and *H* representing the call to either Gini Impurity or Entropy, the formula for the Information Gain is as follows:\n",
    "\n",
    "$$ IG(T,\\alpha) = H(T) - H(T | \\alpha) $$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Visual Example: Boolean Logic"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this example, you will modify the example and answers vector so that a decision tree can be built from it. Once the tree is built, The examples list will be fed back through the tree to ensure it is working appropriately. This example will use the following attributes and the default table is below:\n",
    "\n",
    "|           | V0 | AND | OR | NAND | XOR | Result |\n",
    "| :---: | :---: | :---: | :---: | :---: | :---: | :---: |\n",
    "| Example 1 | 1  |  1  | 1  |  0   |  1  |   0    |  \n",
    "| Example 2 | 1  |  1  | 0  |  0   |  0  |   1    |\n",
    "| Example 3 | 1  |  0  | 1  |  1   |  1  |   1    |\n",
    "| Example 4 | 0  |  1  | 0  |  1   |  0  |   1    |\n",
    "| Example 5 | 0  |  0  | 0  |  1   |  1  |   0    |"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "4d54791281c64a80b1088121cf9739b6",
       "version_major": 2,
       "version_minor": 0
      },
      "text/html": [
       "<p>Failed to display Jupyter Widget of type <code>VBox</code>.</p>\n",
       "<p>\n",
       "  If you're reading this message in the Jupyter Notebook or JupyterLab Notebook, it may mean\n",
       "  that the widgets JavaScript is still loading. If this message persists, it\n",
       "  likely means that the widgets JavaScript library is either not installed or\n",
       "  not enabled. See the <a href=\"https://ipywidgets.readthedocs.io/en/stable/user_install.html\">Jupyter\n",
       "  Widgets Documentation</a> for setup instructions.\n",
       "</p>\n",
       "<p>\n",
       "  If you're reading this message in another frontend (for example, a static\n",
       "  rendering on GitHub or <a href=\"https://nbviewer.jupyter.org/\">NBViewer</a>),\n",
       "  it may mean that your frontend doesn't currently support widgets.\n",
       "</p>\n"
      ],
      "text/plain": [
       "VBox(children=(HBox(children=(Label(value='Test Result:'), Text(value=''))), Button(description='Run Algorithm', style=ButtonStyle())))"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "import numpy as np\n",
    "import ipywidgets as widgets\n",
    "from DecisionTree import DecisionTree\n",
    "import Example1Widgets as ew1\n",
    "\n",
    "Examples = [\n",
    "    [1,1,1,0,1],\n",
    "    [1,1,0,0,0],\n",
    "    [1,0,1,1,1],\n",
    "    [0,1,0,1,0],\n",
    "    [0,0,0,1,1]\n",
    "]\n",
    "\n",
    "Answers = [\n",
    "    0,1,1,1,0\n",
    "]\n",
    "\n",
    "def RunExample1(b):\n",
    "    tree = DecisionTree()\n",
    "    tree.BuildTree(np.array(Examples),np.array(Answers))\n",
    "    answer = tree.classify(np.array(Examples))\n",
    "    ew1.solutionWidget.value = str(answer)\n",
    "    \n",
    "ew1.runButton.on_click(RunExample1)\n",
    "widgets.VBox([ew1.bx1,ew1.runButton])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Aeromechanical Example: Classifying Data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In aeromechanical testing, it would be helpful if data could be classified automatically. By using methods previously discussed, we can extract the features of the data. By using these features, we can build a decision tree for them. For example, we can build a decision tree classifying the data as either Integral Vibratory response, Non-Integral Vibratory response, or No Vibratory response. An example of the features that may be used is as follows:\n",
    "\n",
    ">  \n",
    "1. Does it have a Vibratory Response\n",
    "2. Does the response follow an EO line\n",
    "3. Is there a phase shift of 180 degrees  \n",
    "...\n",
    "\n",
    "These features will be used to build a decision tree used to classify files of data. More complex decision trees can be used for data analysis. These decision trees will split at nodes based on the data itself. For example, a decision tree can be built the first split is based on frequency ranges, the next level split may be the magnitude of the data, and the last split can be a requirement of a 180 degree phase shift. If all of the conditions are met, the classification could be an Integral response.\n",
    "\n",
    "This example for simlicity would end up being very similar to example 1. Therefore, In this lesson, you are challenged to create a decision tree that is non-binary and can split based on the examples you come up with. Try to create a decision tree for a scenario you use to classify data in your work. Also, please reference and use the example code supplied in the Summary section."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Summary"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this lesson, we learned about decision trees. We first defined decision trees, then explained the algorithm, and finally went through several examples. We looked at how to create a decision tree through decision tree learning. We looked at the algorithms for entropy, gini impurity, and information gain which is used to determine which nodes to split on at each level of the tree. We looked at an example of using and creating the decision tree in code. Finally we looked at the applications in aeromechanical testing. \n",
    "\n",
    "The code used for the examples is downloadable below:\n",
    "\n",
    "[DecisionTree.py](./DecisionTree.py)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## References"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Russel, S. J., & Norvig, P. (2015). Learning From Examples. In Artificial intelligence: A modern approach (3rd ed., pp. 697-713). Noida, India: Pearson India Education Services Pvt.\n",
    "\n",
    "Decision tree learning. (2018, July 11). Retrieved from https://en.wikipedia.org/wiki/Decision_tree_learning"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
